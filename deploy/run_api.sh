#!/usr/bin/env sh
# cria os arquivos de log e começa a rastreá-los
touch /logs/gunicorn.log
touch /logs/gunicorn-access.log
tail -n 0 -f /logs/gunicorn*.log &

python manage.py collectstatic --no-input
python manage.py makemigrations
python manage.py migrate

exec gunicorn api.wsgi:application \
    --name api-wedev \
    --bind 0.0.0.0:8000 \
    --workers 5 \
    --log-level=info \
    --log-file=/logs/gunicorn.log \
    --access-logfile=/logs/gunicorn-access.log
"$@"
