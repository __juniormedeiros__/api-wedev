# API Telegram WeDev

Principais dependências:

![](https://img.shields.io/badge/Django-3.2.5-critical)
![](https://img.shields.io/badge/djangorestframework-3.12.4-important)
![](https://img.shields.io/badge/Jinja2-3.0.1-success)
![](https://img.shields.io/badge/requests-2.26.0-critical)
![](https://img.shields.io/badge/drf__flex__fields-0.9.1-critical)
![](https://img.shields.io/badge/django-filter==2.4.0-critical)
![](https://img.shields.io/badge/gunicorn-20.1.0-critical)


A API Telegram da WeDev tem como o intuito servir como backend para aplicações de cadastros de usuários e mensagens
que são enviadas ao Telegram.

## Algumas funcionalidades 

- Bot no Telegram para aceite dos termos (**WeDevChatBot**)
- Listagem e cadastro de **usuários** do Telegram   `` api/v1/users``
- Listagem e cadastro de **mensagens** a ser enviada para o Telegram`` api/v1/messages``


## Ambiente de desenvolvimento

Pré-requisitos:

- Python na versão especificada no projeto, baixar e instalar em:
  
> [Windows](https://www.python.org/downloads/windows/)

> [Mac](https://www.python.org/downloads/mac-osx/)

> [Linux](https://www.python.org/downloads/source/)


Após fazer o clone do projeto por ssh ou https: 

- ssh: ``git clone git@bitbucket.org:__juniormedeiros__/api-wedev.git``
- https: ``git clone https://__juniormedeiros__@bitbucket.org/__juniormedeiros__/api-wedev.git``

Acessar os fontes do projeto:

    $ cd api-wedev

Criar diretório de logs:

    $ mkdir logs

Instalação da virtualenv:

[https://virtualenv.pypa.io/en/latest/installation.html](https://virtualenv.pypa.io/en/latest/installation.html)

Criação da virtualenv para isolar a instalação das dependências:

    $ virtualenv .venv

Ativação da virtualenv:

    $ source .venv/bin/activate

Realizar a instalação das dependências do projeto:

    $ pip install -r src/requirements.txt

Configurar as variáveis de ambiente para desenvolvimento:

| Ambientes              | Descrição                                            |
| ---------------------- | --------------------------------------------------   |
| PRODUCTION             | Define o uso do API em ambiente de Produção          |
| SANDBOX                | Define o uso da API em ambiente de Testes            |
| DEVELOPMENT            | Define o uso da API em ambiente de Desenvolvimento   |


**IMPORTANTE**: Para o ambientes de produção é utilizado o arquivo  ***src/deploy/envs*** [production.yml] que
define as configurações de variáveis de ambiente e a instância do container DOCKER que executará.

### Ambiente

| Variável    | Descrição                          |
| ----------- | ---------------------------------- |
| ENVIRONMENT | DEVELOPMENT, SANDBOX ou PRODUCTION |

Antes de subir a aplicação é necessário gerar as migrations para criação do banco de dados conforme o 
tipo de **ambiente** escolhido.

    $ python manage.py makemigrations --settings settings.<ENVIRONMENT>

Aplicando as migrações a base de dados escolhida.

    $ python manage.py migrate --settings settings.<ENVIRONMENT>

Criando usuário para acesso a área do admin.

    $ python manage.py createsuperuser --settings settings.<ENVIRONMENT>

Neste momento, já se pode rodar o comando abaixo para executar o projeto no ambiente desejado:

    $ python manage.py runserver --settings settings.<ENVIRONMENT>

### Registrando Aplicação de Autenticação - OAuth2

Acessamos http://<YOUR_URL>:8000/admin e nos autenticamos no admin do django, agora estamos prontos para registrar nossa API de autenticação,
conforme imagem de exemplo abaixo.

* **OBS:** Não altere os campos **Client id** e **Client secret**, eles são autogerados.

![](docs/RegistroApp.png)

- Pronto! Agora temos nossa API de autenticação funcional, vamos testá-la? Para isso vamos executar o seguinte comando:

``` bash
  $ curl -X POST <YOUR_URL>:8000/api/v1/token/ 
         -H "content-type: application/x-www-form-urlencoded" 
         -d "grant_type=password&client_id=<your client id>&client_secret=<your client secret>&username=<your username>&password=<your password>"
```

* A resposta deve ser a seguinte:
``` python
   { 
      "expires_in": 36000, 
      "refresh_token": <your refresh token>, 
      "access_token": <your access token>, 
      "token_type": "Bearer", 
      "scope": "read write groups"
   }
```

* O campo **expires_in** define o tempo em segundos da validade do token.

### Estrutura do projeto

Estrutura de diretórios / arquivos:

```
.
├── README.md
├── .gitignore
├── deploy/
│   ├── run_api.sh
│   ├── envs/
│   │   └── production.yml
│   └── images/
│       └── Dockerfile
├── doc/
│   ├── DiagramaBaseDados.png
│   ├── DiagramaSequencia.png
│   ├── RegistroApp.png
│   ├── Telegram API - WeDev.postman_environment.json
│   └── Telegram API - WeDev.postman_collection.json
└── src/
    ├── __init__.py
    ├── manage.py
    ├── settings/
    │   ├── __init__.py
    │   ├── development.py
    │   ├── production.py
    │   └── sandbox.py
    ├── requirements.txt
    └── api/
        ├── __init__.py
        ├── asgi.py
        ├── urls.py
        ├── wsgi.py
        ├── celery.py
        └── v1/
            ├── managers/
            ├── migrations/
            ├── models/
            ├── serializers/
            ├── services/
            ├── tests/
            └── views/      
```

### Lista de EndPoints:

- api/v1/token
- api/v1/users
- api/v1/users/**<pk_user>**
- api/v1/users?accept_terms=**<true_or_false>**
- api/v1/messages
- api/v1/messages/**<pk_message>**


### Macro do processo

* Como premissa os EndPoints abaixo utilizam autenticação OAuth2 para qualquer requisição, sendo necessário
passar o Authorization Token no header da requisição, em caso de dúvidas instale o [Postman](https://www.postman.com/downloads/) e importe as colections da pasta doc/ *.json

a) O EndPoint /api/v1/users - é o responsável pela listagem e inclusão de usuários vindos do Telegram:

``` python
[
    {
        "id": 3,
        "chat_id": 462170282,
        "message_id": 315,
        "username": "rodrigo_silveira",
        "first_name": "rodrigo",
        "last_name": "silveira",
        "accept_terms": true,
        "created_at": "2021-08-20T18:15:30.269148-03:00"
    }
]
```
- Os métodos permitidos nesse endpoint são GET e POST, o método GET busca todos os usuários cadastrados na base e o POST grava
o usuário na base de dados para posterior consulta e envio da mensagem no endpoint de **api/v1/messages**

b) O EndPoint /api/v1/users/**<pk_user>** - é o responsável pela listagem dos usuários cadastrados no telegram pelo **id**:

``` python
    {
        "id": 3,
        "chat_id": 462170282,
        "message_id": 315,
        "username": "rodrigo_silveira",
        "first_name": "rodrigo",
        "last_name": "silveira",
        "accept_terms": true,
        "created_at": "2021-08-20T18:15:30.269148-03:00"
    }
```

c) O EndPoint **/api/v1/messages** - é o responsável pela listagem, envio e armazenamento da mensagem do Telegram.

``` python
    {
        "id": 1,
        "text": "Obrigado novamente!",
        "sent": true,
        "created_at": "2021-08-20T19:19:49.021515-03:00",
        "user_id": 1
    }
```

- Os metódos permitidos nesse endpoint são GET e POST, o GET busca todos os usuários cadastrados e o POST grava a mensagem e
envia para o Telegram conforme o código armazendo no cadastro do usuário.

d) O EndPoint /api/v1/messages/**<pk_message>** - Responsável pela listagem de uma mensagem por **id**:

``` python
    {
        "id": 1,
        "text": "Obrigado novamente!",
        "sent": true,
        "created_at": "2021-08-20T19:19:49.021515-03:00",
        "user_id": 1
    }
```
- Nesse endpoint é possível visualizar também a data de criação do contato e quando ele foi alterado.

e) O EndPoint /api/v1/users?accept_terms<true_or_false> - é o responsável por listar todas os usuários cadastrados
filtrando pelo atributo **accept_terms (Aceite dos termos)** vide documentação no diretório **docs/**


Diagrama de sequência:

![](docs/DiagramaSequencia.png)

Diagrama da Base de Dados:

![](docs/DiagramaBaseDados.png)

### Release / Deploy

Após configurar o projeto você deve primeiro gerar a imagem Docker **image-telegram:latest**:

    $ docker build -t image-telegram -f deploy/images/Dockerfile .

Com a imagem gerada pode executar o docker-compose para iniciar os containers.

    $ docker-compose -f deploy/envs/production.yml up

> Nesse momento a API deve iniciar na porta 8000 do servidor, além do serviços de banco de dados, Telegram, Broker e Workers do Celery.

### Containers

* db-postgres: O serviço **postgres** é a instância da base de dados;
* service-telegram: O serviço **service-telegram** é o serviço que faz comunicação com o Telegram;
* api-telegram: O serviço **api-telegram** é a API que receberá as mensagens grava na base e enviará para o Telegram;
* celery: O serviço **celery** é o Worker responsável pela fila de mensagens.
* redis: O serviço **redis** é o Broker responsável pelo armazenamento da fila de mensagens.

### Request / Collections

Para facilitar a manutenção ou utilização dos EndPoint abaixo segue as collections do Postman.

- [Collection do Postamn](docs/TelegramAPIWeDev.postman_environment.json)
- [Environment do Postamn](docs/TelgramAPIWeDev.postman_collection.json)

### Documentação

Dentro da pasta Docs é possível visualizar arquivos com as documentações:

- [Diagrama de Sequências](docs/DiagramaSequencia.png)

#### Para o cliente

Para iniciar o processo de aceite dos termos, é necessário acessar o **WeDevChatBot** no Telegram:

![Telegram1.png](docs/Telegram1.png)

Em seguida clicar no botão **/start** ou enviar uma mensagem com este mesmo texto.

Ao iniciar a interação com o Bot, receberá a seguinte mensagem: **Aceita nossos termos?**

![Telegram2png.png](docs/Telegram2.png)

* Clicando em sim, será apresentado a mensagem: **Obrigado por aceitar nossos termos!**
* Clicando em não, será apresentado a mensagem: **Tudo bem, obrigado!**

**OBS:** O seu aceite poderá ser aplicado apenas uma vez. Na tentativa de novo aceite o Telegram apresentará
a seguinte mensagem:  **Aceite realizado anteriormente!**

#### Para o suporte

DEFINIR