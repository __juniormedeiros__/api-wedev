from django.db import models
from ..models import telegram_user
from django.core.exceptions import ObjectDoesNotExist


class TelegramUserQuerySet(models.QuerySet):
    def safe_get(self, **kwargs):
        try:
            return telegram_user.TelegramUser.objects.get(**kwargs)
        except ObjectDoesNotExist as e:
            return None
