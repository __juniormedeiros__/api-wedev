from oauth2_provider.settings import oauth2_settings
from oauth2_provider.models import get_access_token_model, get_application_model
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APITestCase
from django.shortcuts import resolve_url as r

Application = get_application_model()
AccessToken = get_access_token_model()
UserModel = get_user_model()


class TelegramUserTest(APITestCase):

    def setUp(self) -> None:
        oauth2_settings._SCOPES = ["read", "write", "scope1", "scope2", "resource1"]

        self.test_user = UserModel.objects.create_user("test_user", "test@example.com", "123456")

        self.application = Application.objects.create(
            name="Telegram API Test",
            user=self.test_user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )

        self.access_token = AccessToken.objects.create(
            user=self.test_user,
            scope="read write",
            expires=timezone.now() + timezone.timedelta(seconds=3600),
            token="secret-access-token-key",
            application=self.application
        )

        self.access_token.save()

        self.auth = "Bearer {0}".format(self.access_token.token)

    def post_save(self, chat_id, accept_terms):
        data = {
            'chat_id': chat_id,
            'message_id': 14,
            'username': 'fabricio_pereira',
            'first_name': 'Fabricio',
            'last_name': 'Pereira',
            'accept_terms': accept_terms
        }

        response = self.client.post(
            r('users-list'),
            data=data,
            HTTP_AUTHORIZATION=self.auth,
            format='json'
        )

        return response.json()

    def test_post_user(self):
        user = self.post_save(462170282, True)
        self.assertEqual(user['id'], 1)

    def test_get_users(self):
        self.post_save(462170285, True)
        self.post_save(462170282, False)
        response = self.client.get(r('users-list'), HTTP_AUTHORIZATION=self.auth).json()
        self.assertEqual(len(response), 2)

    def test_must_be_accept_terms_filter(self):
        self.post_save(462170285, True)
        self.post_save(462170282, False)

        response = self.client.get(
            r('users-list') + '?accept_terms=true',
            HTTP_AUTHORIZATION=self.auth
        ).json()

        self.assertEqual(len(response), 1)
