from oauth2_provider.contrib.rest_framework import OAuth2Authentication, TokenHasReadWriteScope
from rest_condition import Or
from rest_flex_fields.views import FlexFieldsMixin
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser
from django_filters import rest_framework as filters

from ..models.telegram_message import TelegramMessage
from ..serializers.telegram_message import TelegramMessageSerializer

from ..services import telegram_bot


class TelegramMessageView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = TelegramMessage.objects.all()
    serializer_class = TelegramMessageSerializer
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
    filter_backends = (filters.DjangoFilterBackend,)
    http_method_names = ['get', 'post']

    def perform_create(self, serializer):
        chat_id = serializer.validated_data['user_id'].chat_id
        text = serializer.validated_data['text']
        serializer.save(sent=False)
        telegram_bot.send_new_message.delay(chat_id, text)
        serializer.save(sent=True)
