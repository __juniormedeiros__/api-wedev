import django_filters
from rest_flex_fields.views import FlexFieldsMixin
from rest_framework.authentication import SessionAuthentication
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from django_filters import rest_framework as filters
from rest_condition import Or

from ..models.telegram_user import TelegramUser
from ..serializers.telegram_user import TelegramUserSerializer

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication


class TelegramUserFilter(django_filters.FilterSet):
    class Meta:
        model = TelegramUser
        fields = ['accept_terms']


class TelegramUserView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = TelegramUser.objects.all()
    serializer_class = TelegramUserSerializer
    authentication_classes = [OAuth2Authentication, SessionAuthentication]
    permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TelegramUserFilter
    http_method_names = ['get', 'post']
