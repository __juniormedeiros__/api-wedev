from .views import telegram_user, telegram_message
from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

router = SimpleRouter(trailing_slash=False)
router.register('users', telegram_user.TelegramUserView, basename='users')
router.register('messages', telegram_message.TelegramMessageView, basename='messages')

urlpatterns = [
    url(r'^', include(router.urls))
]
