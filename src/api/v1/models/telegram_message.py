from django.db import models


class TelegramMessage(models.Model):
    user_id = models.ForeignKey(
        'TelegramUser',
        on_delete=models.CASCADE,
        db_column='user_id',
        related_name='users',
        blank=False,
        null=False
    )
    text = models.TextField(blank=False, null=False)
    sent = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text

    class Meta:
        db_table = 'telegram_message'
        app_label = 'v1'
        managed = True
        ordering = ['id']
