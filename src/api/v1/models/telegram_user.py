from django.db import models
from ..managers.telegram_user import TelegramUserQuerySet


class TelegramUser(models.Model):
    chat_id = models.BigIntegerField(blank=False, null=False, unique=True)
    message_id = models.BigIntegerField(blank=False, null=False)
    username = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100, null=False, blank=False)
    last_name = models.CharField(max_length=100, null=False, blank=False)
    accept_terms = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)

    objects = TelegramUserQuerySet.as_manager()

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'telegram_user'
        app_label = 'v1'
        managed = True
        ordering = ['id']