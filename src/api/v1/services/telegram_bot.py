import os
import telebot
import logging

from celery import shared_task
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from django.forms.models import model_to_dict

from api.v1.services.telegram_service import TelegramService

TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN', '1967874226:AAFtjVthha1AFGjraW3lan_-8TvZoKHgIqU')
bot = telebot.TeleBot(TELEGRAM_TOKEN)

logging.basicConfig(
    filename=os.environ.get('LOG_DIR', '../logs/service-wedev'),
    level=logging.DEBUG
)


def save_send_response(call, accept_terms, text):
    telegram = TelegramService.save_user(call, accept_terms)
    send_new_message(call.message.chat.id, text)
    logging.warning(model_to_dict(telegram))


def gen_markup():
    markup = InlineKeyboardMarkup()
    markup.add(InlineKeyboardButton("Sim", callback_data="cb_yes"),
               InlineKeyboardButton("Não", callback_data="cb_no"))
    return markup


@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    try:
        user = TelegramService.safe_get(chat_id=call.message.chat.id)
        if user:
            bot.answer_callback_query(call.id, "Aceite realizado anteriormente!")
        else:
            if call.data == "cb_yes":
                save_send_response(call, True, "Obrigado por aceitar nossos termos!")
            elif call.data == "cb_no":
                save_send_response(call, False, "Tudo bem, obrigado!")
    except Exception as e:
        logging.critical(str(e))


@bot.message_handler(commands=['start'])
def message_handler(message):
    bot.send_message(message.chat.id, "Aceita nossos termos?", reply_markup=gen_markup())


@shared_task()
def send_new_message(chat_id, message):
    try:
        bot.send_message(chat_id=chat_id, text=message)
        return True
    except Exception as e:
        return False


def polling():
    bot.polling(none_stop=True)
