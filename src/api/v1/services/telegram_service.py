import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.development')
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from ..models.telegram_user import TelegramUser


class TelegramService:

    @staticmethod
    def safe_get(**kwargs):
        return TelegramUser.objects.safe_get(**kwargs)

    @staticmethod
    def save_user(call, accept_terms):
        telegram = TelegramUser(
            chat_id=call.message.chat.id,
            message_id=call.message.id,
            username=call.from_user.username,
            first_name=call.from_user.first_name,
            last_name=call.from_user.last_name,
            accept_terms=accept_terms
        )
        telegram.save()
        return telegram
