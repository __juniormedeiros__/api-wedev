from rest_flex_fields import FlexFieldsModelSerializer
from ..models.telegram_user import TelegramUser


class TelegramUserSerializer(FlexFieldsModelSerializer):

    class Meta:
        model = TelegramUser
        fields = '__all__'
        read_only_fields = ('id', 'created_at', 'date_message')
