from rest_flex_fields import FlexFieldsModelSerializer
from ..models.telegram_message import TelegramMessage


class TelegramMessageSerializer(FlexFieldsModelSerializer):

    class Meta:
        model = TelegramMessage
        fields = '__all__'
        read_only_fields = ('id', 'sent', 'created_at')
